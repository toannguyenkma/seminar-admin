import React from "react";
import {
    HydraAdmin,
    FieldGuesser,
    ListGuesser,
    ResourceGuesser,
    fetchHydra, hydraDataProvider,
    EditGuesser,
    InputGuesser,
    CreateGuesser
} from "@api-platform/admin";
import {
    ReferenceField, TextField, ReferenceManyField, SingleFieldList, ChipField,
    ReferenceInput, AutocompleteInput, ReferenceArrayInput, AutocompleteArrayInput, SelectArrayInput, SelectInput
} from "react-admin";
import {parseHydraDocumentation} from "@api-platform/api-doc-parser";

// Replace with your own API entrypoint
// For instance if https://example.com/api/books is the path to the collection of book resources, then the entrypoint is https://example.com/api

const entrypoint = 'http://seminar-api-local.com/api';

const dataProvider = hydraDataProvider(
    entrypoint,
    fetchHydra,
    parseHydraDocumentation,
    true // useEmbedded parameter
);

const ProductsList = (props) => (
    <ListGuesser {...props}>
        <FieldGuesser source="name"/>
        {/*<FieldGuesser source="category.name" reference="categories"/>*/}
        {/* Use react-admin components directly when you want complex fields. */}
        <ReferenceField label="category" source="category" reference="categories">
            <TextField source="name"/>
        </ReferenceField>
    </ListGuesser>
);

const ProductsCreate = (props) => (
    <CreateGuesser {...props}>
        <InputGuesser source="name"/>

        <ReferenceInput source="category" reference="categories"
                        filterToQuery={searchText => ({ name: searchText })}
        >
            <SelectInput optionText="name" />
        </ReferenceInput>
    </CreateGuesser>
)
const CategoriesList = (props) => (
    <ListGuesser {...props}>
        <FieldGuesser source="name"/>
        {/*<FieldGuesser source="products"/>*/}
        <ReferenceManyField title="Products" reference="products" target="category">
            <SingleFieldList title="products">
                <ChipField  source="name"/>
            </SingleFieldList>
        </ReferenceManyField>
    </ListGuesser>
);

const CategoriesEdit = (props) => (
    <EditGuesser {...props}>
        <InputGuesser source="name"/>

        <ReferenceArrayInput source="product_ids" reference="products">
            <SelectArrayInput optionText="name" />
        </ReferenceArrayInput>
    </EditGuesser>
)


export default () => (
    <HydraAdmin dataProvider={dataProvider}
                entrypoint={entrypoint}>
        <ResourceGuesser
            name="products"
            list={ProductsList}
            create={ProductsCreate}
        />
        <ResourceGuesser
            name="categories"
            list={CategoriesList}
            edit={CategoriesEdit}
        />
    </HydraAdmin>
);
